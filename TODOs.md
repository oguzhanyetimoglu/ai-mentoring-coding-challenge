# Coding Challenge

Please look into the following coding challenges and solve them as far as possible.
If you have any questions don't hesitate to ask us directly.
Challenges with asterisks ('*') might be harder to solve.
For the following tasks, all code should be placed in functions or methods of classes; not top-level in a python file.
We would like to receive a link to your forked repository 48h (2 days) after you received the link to this repository.

## AI programming questions

#### 1.) Simple conversation
Implement the ```talk()``` method in the class ```GPTNeoModel```.
Stick to the object oriented design.
Function-signatures shall not be changed, but new functions/methods can be added to classes.

The conversation happens between the user (addressed as _Max_) and an AI (addressed as _Donald_).
Mind the ```prompt```, which explicitly  generates a setting for the conversation.

Here is an example of how to call GPT Neo for such a conversation scenario.
In this case the prompt is continued by the generative model, which in turn gives us an answer from Donald:
```python
from transformers import GPTNeoForCausalLM, GPT2Tokenizer
model = GPTNeoForCausalLM.from_pretrained("EleutherAI/gpt-neo-125M")
tokenizer = GPT2Tokenizer.from_pretrained("EleutherAI/gpt-neo-125M")

prompt = "This is a conversation between Max and Donald Trump. They are talking about electric vehicles and how they can make America great again.\n\nMax:I think Tesla is amazing and the US will greatly benefit if we subsidize EVs.\n\nDonald:It’s wonderful to have it as a percentage of your cars, but going fully electric… I think is a mistake.\n\nMax:Why do you think so?\n\nDonald:"
input_ids = tokenizer(prompt, return_tensors="pt").input_ids

gen_tokens = model.generate(input_ids, do_sample=True, temperature=0.9, max_length=300,)
gen_text = tokenizer.batch_decode(gen_tokens)[0]

print(gen_text)
```

The goal is then to have an ongoing conversation like this:

![Animated conversation flow form flow.gif](flow.gif)

Please consider the following:

- Forbid Trump to say the word "cool" (use huggingfaces inbuild function, not some string post-processing)

- Trump's 3rd answer should always include the sentence "If I'm president in 2024 then...". 
Please include it into the model's prediction.

- The class ```ExampleModel``` shall not be modified, your implementation shall be done in the class ```GPTNeoModel```.
The instanciation of ExampleModel _m_ in ```main.py``` should then be replaced with an instanciation of your GPTNeoModel implementation.

#### 2.) Conversation end

As soon as the user (Max) says "bye", the conversation ends.
Donald should not have the last word.
Change the code in ```main.py``` accordingly.

## Object Oriented Programming questions

### 2.) Design Patterns

Implement an observer class ```ModelObserver``` (as _Observer_ role) for objects of the class ```Model```.
```Model``` should take the role of the _Observable_.
Adapt the code of the class ```Model```, so that observers can be registered to it.
Observers should get notified on each call of ```talk()``` and increment an observer-internal counter.
Keep in mind, that one Observable can have multiple Observers.

### * 3.) Python3 libs

#### * 3.1.) logging
Use the Python ```logging``` package to write all program outputs (usages of ```print```) to a single log file.
Feel free, to add further debugging messages anywhere in the code.

#### * 3.2.) CLI arguments
Use ```argparse``` to parse command line arguments, one being "verbose": if set, the log-level should be "DEBUG", if not "INFO".

#### * 3.3.) pip ```requirements.txt```
Create a requirements file for all necessary imports of this program.

### ** 4.) Design Patterns -- contd.

Which (not so common) design pattern is realized by the classes ```Model``` and ```StorageHandler```?  
Why should one use it?
Make some notes, we will talk about this in a follow-up-meeting.
