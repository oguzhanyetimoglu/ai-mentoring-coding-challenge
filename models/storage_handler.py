import json
import os


class StorageHandler:

    __slots__ = ["_storage_path"]

    def __init__(self, storage_dir: str):
        self._storage_path = os.path.join(storage_dir, "example.json")

    def get_path(self) -> str:
        return self._storage_path

    def set_dir(self, new_storage_dir):
        self._storage_path = os.path.join(new_storage_dir, "example.json")

    def set_path(self, new_storage_path: str):
        """
        Sets the new storage path.
        (Use with caution)

        Args:
            new_storage_path: New storage path (Unchecked, must be valid!)
        """
        self._storage_path = new_storage_path

    def __write_to_storage(self, information: dict):

        with open(self._storage_path, 'w') as f:
            json.dump(information, f, indent=4)

    def store(self, model):
        self.__write_to_storage({"type": str(type(model)), "inference_result": model.get_content()})
