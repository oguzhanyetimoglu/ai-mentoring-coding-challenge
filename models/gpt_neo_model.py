from transformers import GPTNeoForCausalLM, GPT2Tokenizer

from .model import Model
from .storage_handler import StorageHandler


class GPTNeoModel(Model):
    def __init__(self, storage_handler: StorageHandler):
        super().__init__(storage_handler)

        self.model = GPTNeoForCausalLM.from_pretrained("EleutherAI/gpt-neo-125M")
        self.tokenizer = GPT2Tokenizer.from_pretrained("EleutherAI/gpt-neo-125M")
        self.bad_words = ["cool"]
        self.third_answer = "If I'm president in 2024 then"
        self.count = 1

    def talk(self, user_input: str) -> str:
        if self.count==3:
            user_input = self.third_answer
        input_ids = self.tokenizer(user_input, return_tensors="pt").input_ids
        bad_words_ids = self.tokenizer(self.bad_words, add_prefix_space=True).input_ids
        gen_tokens = self.model.generate(input_ids, do_sample=True, temperature=0.9, 
                                        bad_words_ids=bad_words_ids , max_length=100)
        gen_text = self.tokenizer.batch_decode(gen_tokens)[0]

        # Omits user input from generated text
        if self.count!=3:
            gen_text = gen_text[len(user_input):]

        self.count += 1
        return gen_text

    def get_content(self):
        pass