from abc import abstractmethod, ABC

from .storage_handler import StorageHandler


class Model(ABC):

    __slots__ = ["_version",
                 "_handler"]

    def __init__(self, handler: StorageHandler):
        self._version = "0.6.19"
        self._handler = handler

    @abstractmethod
    def talk(self, user_input: str) -> str:
        pass

    @abstractmethod
    def get_content(self):
        pass

    def dump(self):
        self._handler.store(self)

class ModelObserver:
    def __init__(self, logger):
        self.counter = 0
        self.model_list = []
        self.logger = logger

    def notify(self, model: Model):
        """
        Called for each `talk()`, take class Model 
        and increment an observer-internal counter
        """
        self.counter += 1
        self.model_list.append(model)

    def get_counter(self):
        return self.counter

    def get_model_list(self):
        return self.model_list

    def print_logger(self):
        log = "Observer: The number of call is {}".format(self.counter)
        self.logger.info(log)
        print(log)


        