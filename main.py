import sys
import logging
import argparse
from os.path import expanduser

from models.example_model import ExampleModel
from models.gpt_neo_model import GPTNeoModel
from models.model import Model, ModelObserver
from models.storage_handler import StorageHandler


def start(model: Model, logger, observe=False):
    """
    Runs gpt neo model

    :param Model: model
    :param logger: logger to write prints 
    :param observe: boolean to check if there is observer or not
        info a file
    """ 
    if observe: 
        observer = ModelObserver(logger)

    while True:
        user_input = input("Max:")
        logger.info("Max: {}".format(user_input))
        if user_input=="bye": 
            break
        response = model.talk(user_input)
        if observe: 
            observer.notify(model)
        print("Donald:{}".format(response))
        logger.info("Donald: {}".format(response))

    if observe:
        observer.print_logger()

    model.dump()


def create_parser():
    """
    Creates argpase and configure parser

    :return: Parsed arguments
    """ 
    parser = argparse.ArgumentParser()
    parser.add_argument('-verbose', action='store_true')
    args = parser.parse_args()
    return args


def create_logger(_args):
    """
    Creates and configures logger 
    If arg is set to "verbose", the log-level should be "DEBUG", if not "INFO"
    
    :param _args: Parsed arguments
    :return: Logger
    """ 
    logging.basicConfig(filename="log_file.log", 
					    format='%(asctime)s %(message)s', 
					    filemode='w') 
    logger=logging.getLogger()
    
    if _args.verbose:
        logger.setLevel(level=logging.DEBUG)
    else:
        logger.setLevel(level=logging.INFO)
    return logger
 

if __name__ == "__main__":

    # Init parser
    args = create_parser()

    # Init logger
    logger = create_logger(args)

    # Storage handler
    sh = StorageHandler(expanduser("~"))
    
    # Model
    m = GPTNeoModel(sh)
    start(m, logger, observe=True)
