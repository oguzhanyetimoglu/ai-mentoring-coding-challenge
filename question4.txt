Which (not so common) design pattern is realized by the classes Model and StorageHandler?
Why should one use it?


1. `Abstract factory` design pattern is realized in the case. This design pattern has an advantage to produce interface for closely related objects without specifying their concrete classes. In our project, `Model` is a abstract interface of `GPTNeoModel`. In order to make it happen, an abstract base class (`ABC`) can be utilized. This pattern makes the object-oriented software flexible and reusable, thereby objects are easier to implement, change, test, and reuse.

2. Also, `Flyweight` design pattern is realized in the case. It allows to share an object between multiple objects, thus `Flyweight` saves RAM by caching the same data used by different objects. In our project, `storage_handler` instance  of `StorageHandler` class can be utilized for multiple models.
